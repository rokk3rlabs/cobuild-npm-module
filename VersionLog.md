# Version Log #

### v0.4.0 - Feb 2020

- Removed cobuild-rackspace-hook.
- Moved mongodb to dev deps.
- Updated mongodb
- Updated mocha
- Updated gulp
- Updated mongoose

### v0.3.4 - May 2019

* Adjust formatSortString to allow the orm parametter to map the sort string

### v0.3.0 - ** [Breaking v0.1.3 and older]** Dec 17 2016  ###

* Update: Standard API Response (ok - 200 ) of API encapsulate response in data object. 


### v0.1.3 - Nov 2016 ###

* Fix unit tests fixtures load function

### v0.1.2 - **[Breaking v0.1.1 and older]** Nov 2016  ###

* Remove absolute path use for path resolution. Avoid issus at GAE deployments
--> Projects using this module should define process.env.NODE_PATH variable with root of project path (ex. NODE_PATH=. node server.js).  

### v0.1.1 - October 2016 ###

* Db utils for lambda functions 
* Files required function with express limiter support
* Unit tests support improved


### v0.1.0 - Initial Release September 2016 ###


