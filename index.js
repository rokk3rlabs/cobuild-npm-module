debug = require('debug')('cobuild2');

var Cobuild = {};
var fs = require('fs');

Cobuild.Error = require('./lib/error');
Cobuild.Policies = require('./lib/policies');
Cobuild.Utils = require('./lib/utils');
Cobuild.Hooks = require('./lib/hooks');
Cobuild.Validators = require('./lib/utils/validators');

Cobuild.config = Cobuild.Utils.Files.requireIfExists('config/config');
Cobuild.paths = require('./lib/paths');


module.exports = Cobuild;