# Cobuild npm module 
Cobuild2 package provides some utilities to make more easy and fast the development on the  [Cobuild Express Stack](https://bitbucket.org/rokk3rlabs/cobuild-express-stack/)

**For more info go to [Cobuild wiki](http://wiki.weco.build/categories/Cobuild-Ecosystem/Cobuild-npm-Module/)**

## Table of contents
* [Installation](https://bitbucket.org/rokk3rlabs/cobuild-npm-module#markdown-header-installation)
* [Requirements](https://bitbucket.org/rokk3rlabs/cobuild-npm-module#markdown-header-requirements)
* [Version log](https://bitbucket.org/rokk3rlabs/cobuild-npm-module#markdown-header-version-log)

## Installation 
If you don't have ssh configuration [CLICK HERE](https://confluence.atlassian.com/bitbucket/set-up-ssh-for-git-728138079.html) and follow the tutorial steps (Recommended)

* Install the package
```
npm install https://bitbucket.org/rokk3rlabs/cobuild-npm-module.git#v0.1.3 --save
```
* Require it in your code
```
var Cobuild = require('cobuild2');
```
## Requirements

* Node js
* ssh configuration

## Version Log 

### v0.3.0 - ** [Breaking v0.1.3 and older]** Dec 17 2016  ###

* Update: Standard API Response (ok - 200 ) of API encapsulate response in data object. 


### v0.1.3 - Nov 2016 ###

* Fix unit tests fixtures load function

### v0.1.2 - **[Breaking v0.1.1 and older]** Nov 2016  ###

* Remove absolute path use for path resolution. Avoid issus at GAE deployments
--> Projects using this module should define process.env.NODE_PATH variable with root of project path (ex. NODE_PATH=. node server.js).  

### v0.1.1 - October 2016 ###

* Db utils for lambda functions 
* Files required function with express limiter support
* Unit tests support improved


### v0.1.0 - Initial Release September 2016 ###

***
### Version
0.3.0 