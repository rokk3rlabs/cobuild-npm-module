var pathPackage = require('path');
var Cobuild = require(__dirname+'/files.js');

var app_root = pathPackage.resolve(process.env.NODE_PATH);
	
function loadFixtures(path){
	var fixtures = {};
	var folder = pathPackage.dirname(path);
	try{
		Cobuild.listFilesInFolder( pathPackage.join(pathPackage.relative(app_root, folder), 'fixtures'))
		.forEach(function (file) {
		  var completePath =   pathPackage.join(folder, 'fixtures',  file)
		  fixtures[pathPackage.basename(file, '.js')] = require(completePath);
		});
	}catch(error){
		console.log("loadFixtures ERROR", error)
	}
	return fixtures;
}
module.exports = {
	loadFixtures : loadFixtures
};