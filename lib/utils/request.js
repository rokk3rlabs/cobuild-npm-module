var _ = require('lodash');
var exports = module.exports;

/**
 * Takes all values of an object with format /expression/ and convert to right RegExp ready for Mongoose
 * @param  {Object} queryString JS Object (key value)
 * @return {Object} modified object with RegExp values, where applies
 */
exports.formatQueryString = function(queryString){
  
  var data = _.clone(queryString);
  _.forEach(data, function(value, key){
      
      if(/^\/[^\/]+\//.test(value)){
        value = value.replace(/\//g,'');
        data[key] = new RegExp(value, "i")
      }

  });

  return data;
  
};

/**
 * Takes a string with the format field1 asc, field2 desc and produces an object to be used on mongoose
 * @param  {String} sortString string with the format field1 asc, field2 desc
 * @return {Object} Returns an object with format `{field1:'asc', field2:'desc'}`
 */
exports.formatSortString = function(sortString, orm = 'mysql'){

  var sortObject = {};

  if(!sortString){
    return sortObject;
  }
  
  var sortFields = sortString.split(',');
  _.forEach(sortFields, function(sortField){
    var fieldValue = sortField.split(' ');
    if(fieldValue.length == 2){

      let fieldVal = fieldValue[1]
      if(orm == 'mongo'){

         fieldVal = fieldValue[1] == 'asc' ? 1 : -1;
      }
      sortObject[fieldValue[0]] = fieldVal;
    }
    
  });

  return sortObject

};