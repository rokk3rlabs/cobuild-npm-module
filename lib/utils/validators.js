'use strict';

var _ = require('lodash');
var exports = module.exports;


exports.emptyFields = function emptyFields(fieldRules, bodyData){

	var errors = {};

	_.forEach( fieldRules, function( field ){

		if( _.isEmpty( bodyData[field] ) ){

			errors[field] = field+"_its_empty";

		}

	});

	if( !_.isEmpty(errors) ){
		return { code: 10, userMessage: "empty_fields", serverInfo: "empty_fields", data: errors }
	}else{
		return;
	}

}


exports.fieldsFormat = function fieldsFormat(bodyData){

	var errors = {};

	_.forEach( bodyData, function( fieldValue, field ){

		switch(field) {
		    case "email":

    			var regex = /^(([^<>()\[\]\.,;:\s@\"]+(\.[^<>()\[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
				if( !regex.test( fieldValue.trim() ) ) {
					errors['email'] = { code: 12, serverInfo: "wrong_email_format" };
				}

		        break;
		    
		    default:
		        break;
		}

	});

	if( !_.isEmpty(errors) ){
		return { code: 11, userMessage: "wrong_format", serverInfo: "wrong_format", data: errors }
	}else{
		return;
	}

}
