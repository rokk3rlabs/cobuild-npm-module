var fs = require('fs'),
  async = require('async'),
	_ = require('lodash');

var iterateCollection = function(collection, match, willRemoveFromMatch, pageSize, execute, cb){

    if(!match){
      match = {}
    }
    collection.find(match).count(function(err, total){  
      if(!err){
        debug("total items:", total)
        var totalPages = (total) ? Math.ceil(total/pageSize) : 0;
        
        debug("total pages:", totalPages)

        async.eachSeries(_.range(totalPages), function(page, nextPage){

          var skipAmount = (willRemoveFromMatch) ? 0 : page * pageSize;

          collection.find(match)
                        .limit(pageSize)
                        .skip(skipAmount)
                        .sort({createdAt:1})
                        .toArray(function(err, items){

            async.eachSeries(items, function(item, nextItems){

              execute(item, function(err,result){
                nextItems();
              });         

            },function(err){
              nextPage();
            });

          }); 

        },function(err){ 
          
          
          debug("Update finish.")
          cb(err);

        }); 

      }else{
        debug("Error ", err)
        cb(err);
      }

    }); //initial count   
}

var dbFunctions = {
  'iterateCollection':iterateCollection
};

module.exports = dbFunctions;