var fs = require('fs');
var pathPackage = require('path');

var app_root = pathPackage.resolve(process.env.NODE_PATH);
debug('app_root ', app_root)

var listFilesInFolder = function (path, extension) {

  debug('listFilesInFolder ', path, extension);  
  var completePath = filesFunctions.resolvePath(path);
  return fs.readdirSync(completePath)
  .filter(function (file) {
    debug("file:", file);
    return filterFiles(file, extension);
  });
};

var listFoldersInFolder = function (path) {
  debug('listFoldersInFolder ', path);  
  var completePath = filesFunctions.resolvePath(path);
  return fs.readdirSync(completePath)
  .filter(function (folder) {
    debug('Folder found ', folder, pathPackage.join(completePath, folder));
    return fs.statSync(pathPackage.join(completePath, folder)).isDirectory();
  });
};

var requireFilesInFolder = function (path, app) {
  debug("requireFilesInFolder", path)
  filesFunctions.listFilesInFolder(path)
  .forEach(function (file) {    
    debug("going to require:", pathPackage.join(app_root, path, file));
    require(pathPackage.join(app_root, path, file))(app);
  });
};
 
var requireFilesInFolderWithLimiter = function (path, app, limiter) {
  debug('requireFilesInFolderWithLimiter ', path); 
  filesFunctions.listFilesInFolder(path)
  .forEach(function (file) {    
    debug("going to require:", pathPackage.join(app_root, path, file));
    require(pathPackage.join(app_root, path, file))(app, limiter);
  });
};

var requireFilesInFolderWithLimiter = function (path, app, limiter) {
  filesFunctions.listFilesInFolder(path)
  .forEach(function (file) {
    
    debug("going to require:", pathPackage.join(process.cwd(), path, file));
    require(pathPackage.join(process.cwd(), path, file))(app, limiter);
  });
};

var filterFiles = function  (fileName, extension) {
  
  extension = extension || 'js';
  debug("filterFiles:", pathPackage.extname(fileName) ,  extension)
  return pathPackage.extname(fileName) == ('.' + extension);
};

var absolutePath = function (path) {
  return pathPackage.join(app_root, path);
};

var resolvePath = function (path) {
  debug('resolvePath input: ', path, app_root)
  var completePath = pathPackage.join(app_root,path);
  debug('resolvePath response: ', completePath)
  return completePath;
};

var getModel  = function (modelPath) {
  return filesFunctions.getEntity(modelPath, 'models', true); 
};


var getController  = function (controllerPath) {

  
  return filesFunctions.getEntity(controllerPath, 'controllers', false);
  
};

var getService  = function (servicePath) {

  
  return filesFunctions.getEntity(servicePath, 'services', false);
  
};

var getEntity  = function (entityPath, entityType, toLower) {

  toLower = toLower || false;
  var pathInfo = entityPath.split('.');
  var name = pathInfo.pop();
  var module = pathInfo.join('/');
  
  if(toLower){
    name = name.toLowerCase();
  }
      
  var path = [
    app_root,
    'app',
    'modules',
    module,
    entityType,
    name

  ];
  
  debug("path model:",path.join('/'))
  return require(path.join('/'));
  
};

var requireIfExists = function(path){

  try{
    return require(filesFunctions.resolvePath(path));    
  }catch(exception){
    console.log("\r\n Requiere if exists error :: ", exception);
    return null;
  }
}

var dirpath = function(path){
  return pathPackage.dirname(path);
}

var filesFunctions = {
  'requireFilesInFolderWithLimiter': requireFilesInFolderWithLimiter,
  'requireFilesInFolder':requireFilesInFolder,
  'listFilesInFolder': listFilesInFolder,
  'listFoldersInFolder': listFoldersInFolder,
  'absolutePath': absolutePath,
  'resolvePath': resolvePath,
  'getModel':getModel, 
  'getController':getController,
  'getService': getService,
  'getEntity':getEntity,
  'requireIfExists':requireIfExists,
  'dirpath' : dirpath
};

module.exports = filesFunctions;