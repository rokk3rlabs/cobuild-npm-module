var Cobuild = require('./error');
var debug = require ('debug');

var getPolicy  = function (policyPath) {

  var pathInfo = policyPath.split('.');
    
  if(pathInfo.length >= 2){
    
    var policyName = pathInfo.pop();
    var module = pathInfo.join('/');
    
        
    var path = [
      process.cwd(),
      'app',
      'modules',
      module,
      'policies'

    ];
    
    debug("path policy:",path.join('/'))
    var policy = require(path.join('/'));
    return policy[policyName];

  }else{
    throw Error(Cobuild.Error.InvalidPolicyFile);  
    return false;
  }
    
    
  
};


module.exports = {
  getPolicy: getPolicy
  
};