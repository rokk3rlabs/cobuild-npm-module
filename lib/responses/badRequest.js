var _       = require('lodash');
var i18n    = require('i18n');

/**
 * 400 (Bad Request) Handler
 */

module.exports = function badRequest(err, viewOrRedirect) {

  // Get access to `req` & `res`
  var req = this.req;
  var res = this;

  //Set language to translate user messages
  var language = (req.language == 'es' || req.language == 'en')? req.language : 'en';
  i18n.setLocale(language);
  
  // Serve JSON (with optional JSONP support)
  function sendJSON (errorData) {

    if (!errorData) {
      
      errorData = {
        "code"        : 400,
        "userMessage" : i18n.__("something_went_wrong"),
        "serverInfo"  : "bad_request",
        "data"        : "" 
      }

    }else {
      console.log(errorData, typeof errorData)
      if (typeof errorData == 'string') {
        
        try{
          errorData = JSON.parse(errorData);
        }catch(e){
          errorData = {
            userMessage: errorData,
          }
        }

      } else if( errorData instanceof Error ){
        
        try{
          errorData = JSON.parse(errorData.message);  
        }catch(e){
          
          errorData = {
            userMessage: errorData.message,
            serverInfo: errorData.name,
            data: errorData.errors
          };
        }
        
      }

        
      errorData = { 
        "code"        : errorData.code ? errorData.code : 0,
        "userMessage" : errorData.userMessage ? i18n.__(errorData.userMessage) : "",
        "serverInfo"  : errorData.serverInfo ? errorData.serverInfo : "",
        "data"        : errorData.data ? errorData.data : "" 
      };

    }

    return res.json({ error: errorData });

  }

  // Set status code
  res.status(400);

  // If the user-agent wants JSON, always respond with JSON
  if (req.wantsJSON) {
    return sendJSON(err);
  }

  // Make data more readable for view locals
  var locals;
  if (!err) { locals = {}; }
  else if (typeof err !== 'object'){
    locals = {error: err};
  }
  else {
    var readabilify = function (value) {
      if (_.isArray(value)) {
        return _.map(value, readabilify);
      }
      else if (_.isPlainObject(value)) {
        return value;
      }
      else return value;
    };
    locals = { error: readabilify(err) };
  }

  // Serve HTML view or redirect to specified URL
  if (typeof viewOrRedirect === 'string') {
    if (viewOrRedirect.match(/^(\/|http:\/\/|https:\/\/)/)) {
      return res.redirect(viewOrRedirect);
    }
    else return res.view(viewOrRedirect, locals, function viewReady(viewErr, html) {
      if (viewErr) return sendJSON(err);
      else return res.send(html);
    });
  }
  else return sendJSON(err);

};
