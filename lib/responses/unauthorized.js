var _     = require('lodash');
var i18n  = require('i18n');

/**
 * 401 (Bad Request) Handler
 */

module.exports = function unauthorized(err, viewOrRedirect) {

  // Get access to `req` & `res`
  var req = this.req;
  var res = this;

  //Set language to translate user messages
  var language = (req.language == 'es' || req.language == 'en')? req.language : 'en';
  i18n.setLocale(language);

  // Serve JSON (with optional JSONP support)
  function sendJSON (data) {

    if (!data) {

      data = {
        "code": 401,
        "userMessage" : i18n.__("login_required"),
        "serverInfo"  : "unauthorized_request",
        "data"        : "" 
      }
      
    }else {

      if (typeof data !== 'object' || data instanceof Error) {

        data.data = ( data instanceof Error ) ? data.message : data;
        data.code = "";
        data.userMessage = "";
        data.serverInfo = "";
        
      }else{

        data = { 
          "code"        : data.code ? data.code : 0,
          "userMessage" : data.userMessage ? i18n.__(data.userMessage) : "",
          "serverInfo"  : data.serverInfo ? data.serverInfo : "",
          "data"        : data.data ? data.data : "" 
        };

      }
      
    }

    return res.json({ error: data });

  }

  // Set status code
  res.status(401);

  // If the user-agent wants JSON, always respond with JSON
  if (req.wantsJSON) {
    return sendJSON(err);
  }

  // Make data more readable for view locals
  var locals;
  if (!err) { locals = {}; }
  else if (typeof err !== 'object'){
    locals = {error: err};
  }
  else {
    var readabilify = function (value) {
      if (_.isArray(value)) {
        return _.map(value, readabilify);
      }
      else if (_.isPlainObject(value)) {
        return value;
      }
      else return value;
    };
    locals = { error: readabilify(err) };
  }

  // Serve HTML view or redirect to specified URL
  if (typeof viewOrRedirect === 'string') {
    if (viewOrRedirect.match(/^(\/|http:\/\/|https:\/\/)/)) {
      return res.redirect(viewOrRedirect);
    }
    else return res.view(viewOrRedirect, locals, function viewReady(viewErr, html) {
      if (viewErr) return sendJSON(err);
      else return res.send(html);
    });
  }
  else return sendJSON(err);
};
