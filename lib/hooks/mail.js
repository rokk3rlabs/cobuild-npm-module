
module.exports = function mailHook(options){

  var hookInstance = null;

  if((typeof options === 'object') && options.module && options.credentials){
    var hook = require(options.module);
    
    try{

      if(options.service && hook[options.service] != null){
        hookInstance = hook[options.service](options.credentials);
      }else{
        hookInstance = hook(options.credentials);
      }
      
      if(!hookInstance.send){
        throw Error('Invalid hook instance, should have send method');
        return null;
      }

      return hookInstance;
    }catch(exception){
      console.log("mail hook error:", exception, exception.stack);
      return null;
    }

  }else{
    throw Error('Invalid hook initialization');
    return null;
  }

};