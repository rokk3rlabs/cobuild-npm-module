
module.exports = function uploadHook(options){

  var hookInstance = null;
  
  if((typeof options === 'object') && options.module && options.credentials){
    
    
    var hook = require(options.module);
    
    try{
      if(options.service && hook[options.service] !== null ){
        
        hookInstance = hook[options.service](options.credentials);
      }else{
        hookInstance = hook(options.credentials);      
      }

      if(!hookInstance.upload || !hookInstance.destroy){
        throw Error('Invalid hook instance, should have upload and destroy methods');
        return null;
      }

      return hookInstance;
    }catch(exception){
      console.log("upload hook instance error:", exception, exception.stack);
      return null;
    }

  }else{
    throw Error('Invalid hook initialization');
    return null;
  }

  

};