var root = process.cwd();
var path = require('path');

module.exports = {
  app: path.join(root,'/app'),
  config: path.join(root,'/config'),
  hooks: path.join(root,'/app/hooks'),
  modules: path.join(root,'/app/modules'),
  public: path.join(root, '/public'), 
  customResponses : path.join(__dirname, '/responses'),
}