var should = require('should');

describe('cobuild-npm-module / strings ', function(){
	var fixtures = {};
  	 loadFixtures();
	it('should get a code', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.code(fixtures.code.len)
		if (response.length == fixtures.code.len){
			console.log('cod: ', response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error ', response);
			response.should.not.be.ok();
			done();
		}
	});
	it('should get random', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.getRandomInt(fixtures.random.min, fixtures.random.max)
		if(response % 1 != 0){
			console.log('Error ', response);
			response.should.not.be.ok();
			done();
		}else{
			console.log('random: ', response);
			response.should.be.ok();
			done();
		}
	});
	it('should get distance', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.getEditDistance(fixtures.getEditDistance.a, fixtures.getEditDistance.b)
		if(response % 1 == 0){
			console.log('distance: ', response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error', response);
			response.should.not.be.ok();
			done();
		}
	});
	it('should guuid', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.guuid()
		if(response){
			console.log('guuid: ', response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error', response);
			response.should.not.be.ok();
			done();
			
		}
	});
	it('should uidLight', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.uidLight(fixtures.code.len)
		if(response){
			console.log('uidLight: ', response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error', response);
			response.should.not.be.ok();
			done();
		}
	});
	it('should uid', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.uid(fixtures.code.len)
		if(response){
			console.log('uid:', response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error', response);
			response.should.not.be.ok();
			done();
		}
	});
	it('should buildPassword', function(done){
		this.timeout(5000);
		var files = require(process.cwd()+'/lib/utils/strings.js');
		var response = files.buildPassword(fixtures.code.len)
		if(response){
			console.log('buildPassword:', response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error', response);
			response.should.not.be.ok();
			done();
		}
	});
	
	function loadFixtures(argument){
		var Cobuild = require(process.cwd()+'/lib/utils/files.js');
       	var path = require('path');
	    Cobuild.listFilesInFolder(__dirname + '/fixtures')
	    .forEach(function (file) {
	  	    var completePath =  path.join(__dirname , 'fixtures',  file)
	        fixtures[file.slice(0, -3)] = require(completePath)
	    });
	}
})
