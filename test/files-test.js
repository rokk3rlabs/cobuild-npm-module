var should = require('should');

describe('cobuild-npm-module / files', function(){
  var fixtures = {};
     loadFixtures();
  it('should list Files In Folder', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    var response = files.listFilesInFolder(fixtures.params.path, fixtures.params.extension)
    if (response.length > 0){
      console.log('There are ',response.length,' files in the folder');
      response.should.be.ok();
      done();
    }else{
      console.log('Error');
      response.should.not.be.ok();
      done();
    }
  });
  it('should list Folder In Folder', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    var response = files.listFoldersInFolder(fixtures.params.path)
    if (response.length > 0){
      console.log('There are ',response.length,' folders in the folder');
      response.should.be.ok();
      done();
    }else{
      console.log('Error');
      response.should.not.be.ok();
      done();
    }
  });
  it('should create a absolute path', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    var response = files.absolutePath(fixtures.params.path);
    if(response ==""){
      console.log('Error in path');
      response.should.not.be.ok();
      done();
    }else{
      console.log('Path', response);
      response.should.be.ok();
      done();
    }
  });
  it('should get a entity', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    console.log(fixtures.params);
    var response = files.getEntity(fixtures.getEntity.entityPath, fixtures.getEntity.entityType, fixtures.getEntity.toLower);
    if (response.length > 0 || response!= null || response!= "undefined"){
      console.log('Was able to get the entity');
      response.should.be.ok();
      done();
    }else{
      console.log('Was not able to get the entity');
      response.should.not.be.ok();
      done();
    }
  });
  it('should get a Controller', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    var response = files.getController(fixtures.getController.path)
    if (response.length > 0 || response!= null || response!= "undefined"){
      console.log('Was able to get the controller');
      response.should.be.ok();
      done();
    }else{
      console.log('It was not possible get the controller');
      response.should.not.be.ok();
      done();
    }
  });

  it('should get a Service', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    var response = files.getService(fixtures.getService.path)
    if (response.length > 0 || response!= null || response!= "undefined"){
      console.log('Was able to get the service');
      response.should.be.ok();
      done();
    }else{
      console.log('Was not able to get the service');
      response.should.not.be.ok();
      done();
    }
  });
  it('should get a Model', function(done){
    this.timeout(5000);
    var files = require(process.cwd()+'/lib/utils/files.js');
    var response = files.getModel(fixtures.getModel.path);
    if (response.length > 0 || response!= null || response!= "undefined"){
      console.log('It was possible get the model');
      response.should.be.ok();
      done();
    }else{
      console.log('It was not possible get the model');
      response.should.not.be.ok();
      done();
    }
  });
  
  function loadFixtures(argument){
    var Cobuild = require(process.cwd()+'/lib/utils/files.js');
        var path = require('path');
      Cobuild.listFilesInFolder(__dirname + '/fixtures')
      .forEach(function (file) {
          var completePath =  path.join(__dirname , 'fixtures',  file)
          fixtures[file.slice(0, -3)] = require(completePath)
      });
  }
})
