var should = require('should');
var async = require('async'),
  mongo = require('mongodb'),
  MongoClient = mongo.MongoClient,
  ObjectID = require('mongodb').ObjectID;

var mongoose = require('mongoose');

describe('cobuild-npm-module / db', function(){
  var fixtures = {};
  var dbObject;
  loadFixtures();

  it('init database with mongodb module', function(done){
    this.timeout(5000);
    initDbMongodb(fixtures.db.dbUrl, function (err, dbOut){
      if (!err){  
        dbObject = dbOut;
        dbOut.should.be.ok();
      } else {
        err.should.not.be.ok();
      }
      done();
    });
  });

  it('should add data to database', function(done){
    this.timeout(5000);
    var collection = fixtures.db.collection;
    var items = fixtures.db.addDataCollection;
    async.eachSeries(items, function(item, nextItems){
      dbObject.collection(collection).insertOne(item, function(err, result){
        if (err){
          err.should.not.be.ok();
        } else {
          result.should.be.ok();
        }
        nextItems()
      });      
    },function(err){
      done();
    });
  });

  it('should iterateCollection to database Mongod', function(done){
    this.timeout(5000);
    var db = require(process.cwd()+'/lib/utils/db.js');
    var collection = fixtures.db.collection;
    
    db.iterateCollection(dbObject.collection(collection), {}, false, 10 ,function(item, cb){   
        item.should.be.ok();
        cb();       
    },function(err,finish){
      if (err){
        err.should.not.be.ok();
      } 
      done();
    });
  });

  it('init database with mongosse module', function(done){
    this.timeout(5000);
    initDbMongose(fixtures.db.dbUrl, function (err, dbOut){
      if (!err){  
        dbOut.should.be.ok();
      } else {
        err.should.not.be.ok();
      }
      done();
    });
  });

  it('should iterateCollection to database Mongosse', function(done){
    this.timeout(5000);
    var db = require(process.cwd()+'/lib/utils/db.js');
    var collection = fixtures.db.collection;

    var testSchema = mongoose.Schema({
      id: String,
      name: String
    });
    var test = mongoose.model(collection, testSchema);
    db.iterateCollection(test, {}, false, 10 ,function(item, cb){   
        item.should.be.ok();
        cb();       
    },function(err,finish){
      if (err){
        err.should.not.be.ok();
      } 
      done();
    });
  });

  it('should drop database', function(done){
    this.timeout(5000);
    dbObject.dropDatabase();
    done();
  });

  function initDbMongodb (dbUrl, cb){
    MongoClient.connect(dbUrl, function(err, dbOut) {
      if(!err){
        cb(err,dbOut);
      }else{
        cb(err);
      }
    });
  }

  function initDbMongose (dbUrl, cb){
    mongoose.connect(dbUrl);
    var db = mongoose.connection;
    db.on('error', function(err){
      cb('Error to connect a Database with Mongosse');
    });
    db.once('open', function(){
      cb(null,mongoose)
    });
  }
  
  function loadFixtures(argument){
    var Cobuild = require(process.cwd()+'/lib/utils/files.js');
        var path = require('path');
      Cobuild.listFilesInFolder(__dirname + '/fixtures')
      .forEach(function (file) {
          var completePath =  path.join(__dirname , 'fixtures',  file)
          fixtures[file.slice(0, -3)] = require(completePath)
      });
  }
})
