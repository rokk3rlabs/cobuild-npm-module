var should = require ('should');

describe ('cobuild-npm-module / hooks',function(){
	var fixtures = {};
	loadFixtures();

	it('should upload file', function (done){
		this.timeout(15000);
		var hooks = require(process.cwd()+'/lib/hooks/upload.js')(fixtures.uploadCredentials);
		var response = hooks.upload('test',__dirname+'/fixtures/'+fixtures.file.upload)
		  response.then(function(result){
		  	result.should.be.ok();
		    console.log("Done", result);
		    done();
		  },function(err){
		  	err.should.not.be.ok();
		  	console.log('err: ', err);
		  	done();
		  }).catch(function(error){
	        console.log('error: ', error);
	        error.should.not.be.ok();
	        done();
      	});
	});

	/*it('should send mail', function (done){
		this.timeout(15000);
		var hooks = require(process.cwd()+'/lib/hooks/mail.js')(fixtures.mailCredentials);
		var response = hooks.send(fixtures.mail, function( err, res){
		if (err){
			err.should.not.be.ok();
		  	console.log('err: ', err);
		  	done();
		}else{
			console.log(res);
			result.should.be.ok();
			done();
		}
		
		
		})
	});*/
	function loadFixtures(argument){
		var Cobuild = require(process.cwd()+'/lib/utils/files.js');
       	var path = require('path');
	    Cobuild.listFilesInFolder(__dirname + '/fixtures')
	    .forEach(function (file) {
	  	    var completePath =  path.join(__dirname , 'fixtures',  file)
	        fixtures[file.slice(0, -3)] = require(completePath)
	    });
	}
})