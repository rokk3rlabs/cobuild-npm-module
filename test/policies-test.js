var should = require('should');

describe ('cobuild-npm-module / polices', function(){
	var fixtures = {};
	loadFixtures();

	it ('should get policy',function(done){
		this.timeout(5000);
		var policy = require(process.cwd()+'/lib/policies.js');
		var response = policy.getPolicy(fixtures.policy.path);
		if (response.length > 0 ){
			console.log('Done',response);
			response.should.be.ok();
			done();
		}else{
			console.log('Error');
			response.should.not.be.ok();
			done();
		}
	});

	function loadFixtures (argument){
		var Cobuild = require(process.cwd()+'/lib/utils/files.js')
		var path = require('path');
	    Cobuild.listFilesInFolder(__dirname + '/fixtures')
	    .forEach(function (file) {
	  	    var completePath =  path.join(__dirname , 'fixtures',  file)
	        fixtures[file.slice(0, -3)] = require(completePath)
	    });
	}

});