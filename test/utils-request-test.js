var should = require('should');

describe('cobuild-npm-module / utils / request ', function(){

	it('should get desc for orm different than mongo', function(done){


		this.timeout(5000);

		var fixture = 'field desc'

		var request = require(process.cwd()+'/lib/utils/request');
		var response = request.formatSortString(fixture)

		let keys = Object.keys(response);
		response[keys[0]].should.be.equal('desc');
		done();

	});

	it('should get desc for orm different than mongo', function(done){


		this.timeout(5000);

		var fixture = 'field asc'

		var request = require(process.cwd()+'/lib/utils/request');
		var response = request.formatSortString(fixture)

		let keys = Object.keys(response);
		response[keys[0]].should.be.equal('asc');
		done();

	});


	it('should get -1 for  mongo', function(done){
		this.timeout(5000);

		var fixture = 'field desc'
		var request = require(process.cwd()+'/lib/utils/request.js');
		var response = request.formatSortString(fixture, 'mongo')

		let keys = Object.keys(response)
		response[keys[0]].should.be.equal(-1)
		done()
	});

	it('should get 1 for  mongo', function(done){
		this.timeout(5000);

		var fixture = 'field asc'
		var request = require(process.cwd()+'/lib/utils/request.js');
		var response = request.formatSortString(fixture, 'mongo')

		let keys = Object.keys(response)
		response[keys[0]].should.be.equal(1)
		done()
	});

})